use serde::{Deserialize};
use serde_json::{json, Value};
use aws_sdk_dynamodb::{Client,types::AttributeValue};
use aws_sdk_xray as xray;
use lambda_runtime::{LambdaEvent, Error as LambdaError, service_fn};

#[derive(Deserialize)]
struct Request {
    wwtp: Option<String>,
    ground_truth: Option<String>,  
    prediction: Option<String>,    
}

#[::tokio::main]
async fn main() -> Result<(), xray::Error> {
    let config = aws_config::load_from_env().await;
    let _client = aws_sdk_xray::Client::new(&config);

    // // Initialize SimpleLogger
    // SimpleLogger::new().with_utc_timestamps().init()?;
    // SimpleLogger::new().with_utc_timestamps().init()?;
    // let subscriber = tracing_subscriber::fmt()
    //     .with_env_filter(EnvFilter::from_default_env())
    //     .with_span_events(FmtSpan::CLOSE)
    //     .finish();
    // tracing::subscriber::set_global_default(subscriber).expect("setting default subscriber failed");

    let _func = service_fn(handler);
    // lambda_runtime::run(func).await?;
    Ok(())
}

async fn handler(event: LambdaEvent<Value>) -> Result<Value, LambdaError> {
    let request: Request = serde_json::from_value(event.payload)?;
    let shared_config = aws_config::load_from_env().await;
    let client = Client::new(&shared_config);

    post_new_info(&client, request.wwtp.clone(), request.ground_truth.clone(), request.prediction.clone()).await?;
    check_prediction(&client, request.wwtp, request.ground_truth, request.prediction).await?;

    Ok(json!({ "message": "Data posted successfully" }))
}

async fn post_new_info(client: &Client, wwtp: Option<String>, ground_truth: Option<String>, prediction: Option<String>) -> Result<(), LambdaError> {
    let table_name = "WWTPInfo";
    let wwtp_av = AttributeValue::S(wwtp.expect("Client (WWTP name) is required"));
    let ground_truth_av = ground_truth.as_ref().expect("Ground truth is required").to_owned();
    let prediction_av = prediction.as_ref().expect("Prediction is required").to_owned();

    client.put_item()
        .table_name(table_name)
        .item("wwtp", wwtp_av.clone())
        .item("ground_truth", AttributeValue::S(ground_truth_av))
        .item("prediction", AttributeValue::S(prediction_av))
        .send()
        .await?;

    Ok(())
}

async fn check_prediction(client: &Client, wwtp: Option<String>, ground_truth: Option<String>, prediction: Option<String>) -> Result<(), LambdaError> {
    let table_name = "WWTPInfo";
    let wwtp_av = AttributeValue::S(wwtp.expect("Client (WWTP name) is required"));
    let ground_truth = ground_truth.as_ref().expect("Ground truth is required");
    let prediction = prediction.as_ref().expect("Prediction is required");

    let correct = if ground_truth == prediction {
        "true"
    } else {
        "false"
    };

    client.update_item()
        .table_name(table_name)
        .key("wwtp", wwtp_av)
        .update_expression("SET correct = :correct")
        .expression_attribute_values(":correct", AttributeValue::S(correct.to_owned()))
        .send()
        .await?;

    Ok(())
}
